CLI Drills Part 1:

Create the following directory structure. (Create empty files where necessary)

hello
├── five
│   └── six
│       ├── c.txt
│       └── seven
│           └── error.log
└── one
    ├── a.txt
    ├── b.txt
    └── two
        ├── d.txt
        └── three
            ├── e.txt
            └── four
                └── access.log


Delete all the files having the .log extension


Add the following content to a.txt


Unix is a family of multitasking, multiuser computer operating systems that derive from the original AT&T Unix, development starting in the 1970s at the Bell Labs research center by Ken Thompson, Dennis Ritchie, and others.


Delete the directory named five.


Rename the one directory to uno.


Move a.txt to the two directory.



Learning Outcomes
You must be able to do the following operations using the command line

Create a directory
Create a file within a particular directory
Navigate through various directories
List all the files in a directory
Copy a file / directory
Move a file / directory
Rename a file
Delete a file
Edit contents of a file
